# Linked-List-Iterator-method-in-Java



import java.util.ListIterator;
import java.util.LinkedList;

public class IterateLinkedListUsingListIteratorExample {
 
 public static void main(String[] args){
  
  //create LinkedList object
  LinkedList IList = new LinkedList();
  
  //add elements to LinkedList
  IList.add("1");
  IList.add("2");
  IList.add("3");
  IList.add("4");
  IList.add("5");
  
  ListIterator itr = IList.listIterator();
  
  System.out.println("Iteraring through elements of Java LinkedList using ListIterator in forward direction...");
  
  while(itr.hasNext())
  {
    System.out.println(itr.next());
  }
  
  System.out.println("Iterating through elements of Java LinkedList using ListIterator in reverse direction...");
  while(itr.hasPrevious())
  System.out.println(itr.previous());
 
 }
}  
  
